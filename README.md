Geoff Creaghan is a Cloudworks Principal delivering Oracle ERP solutions. He began his career in Halifax, Nova Scotia at IBM after graduating from the University of New Brunswick with a degree in computer science.

Website: https://twitter.com/GeoffCreaghan
